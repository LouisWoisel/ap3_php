<?php
    session_start();
    include 'controller.php';
    if(isset($_POST['login']) or !empty($_POST['username']) and !empty($_POST['userpwd'])){
            $username = $_POST['username'];
            $userpwd = $_POST['userpwd'];
            $user = dbGetUser($username,$userpwd);
            //var_dump($user);
            if(empty($user)){
                loginForm();
            }
            else{
                $_SESSION["logged"] = true;
                $_SESSION["user_name"] = $user['user_name'];
                $_SESSION["user_pwd"] = $user['user_pwd'];
                $_SESSION["rules"] = $user['user_rules'];
                require('books.php');
            }
    }
    else{
        loginForm();
    }

    function loginForm(){
        echo '
        <form method="post" action="login.php">
            <input type="text" name="username" placeholder="username"><br>
            <input type="password" name="userpwd" placeholder="Password"><br>
            <input type="submit" name="login">
        </form>
        ';
    }